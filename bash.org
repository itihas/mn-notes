* bash is
- advanced
- arcane
- unreadable
- fails unhelpfully/ungracefully
- irreplaceable.
* google style guide for shell
[[https://google.github.io/styleguide/shell.xml][rohit's link to this]]
* formatting stuff
wrap everything as safely as possible
- @
- *
- ''
- ""
- {}
- $
     
* use printf not echo
  makes life easier

* type
run =man type=
* return values
"${?}" gives you return value
* I/O redirection
- stdin 0
- stdout 1
- stderr 2
- redirects
- pipes
* tee
* grouping
- (;) for in a subshell
- {;} for in the same shell
* conditional expressions
- [] older
- [[]] newer
- (()) for arithmetic
look these up they have arguments that are cool and needed
* loops
- for ... in ---; do { ...; } done
- while ...; do { ...; } done
write in multiple line plz
* variables
three states
- nonexistent
- unset
- set
look up variable and parameter substitutions on bashhackers
* functions
- function foo { ... }
- bar() { ... } use this one it's better for POSIX reasons

- functions take what they're given
- input handling is your job bruh
- reiterate: quotes and brackets /everywhere/

- returns are 0 or error codes
- values need to be echoed out to stdout, and will be caught in variable assignments
  - ret = "$(function 'args')" is a variable assignment

functions can be nested
* async
- < <
- > >

asynchronously redirect stuff

* assoc array
look up later

* switch case
case...in
...;;
...;;
esac

* select
user choice

* links
shellcheck
google styleguids
gnu bash manual
bash-hackers
tldp
gitlab rip-up
http://slides.com/sidharthiyer

* Look up
readlink
dupout
unbuffer
pipefail
getopts, alternative
default IFS in awk and what happens when you unset it.

* regexes

* grep

* todo benchmark sed against awk

* can we transpose a matrix using awk?
