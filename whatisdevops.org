
* Pillars of devops
** Config management
*** Puppet

*** Do it fast vs do it right
  accrue/discharge technical debt
  make it so you can come back later

** Data collection
   know /everything/ about what went wrong
  - System
  - Application
  - Queue

** Alerting
   automate the poring over of the graphs
   Isinga -> Isinga 2
   PagerDuty (!!!)
   exit codes signify alerts. 0, 1, 2 for critical

** CI/CD
   lots of tiny commits > few huge commits
   build, test, deploy for every commit
   debug easier
   Jenkins, rsync, Maven

** Automation
   cron scripts for a bunch of learned/experienced issues
   
** Documentation
   do it right
   *you will forget what you did*
   this is why docs
   try to automate it
   Phabricator
   
* Know thy app, you are the first POC

** DevOps decides on architecture
   Egs:
  1. 2 servers of 16 cores or 4 servers of 8 cores or 8 servers of 4 cores?
     - risk of a server going down is x. in the event of a server going down, remaining servers have to be able to handle traffic. have to overprovision (leave buffer zone on) servers. spread risk out over multiple servers until cost-benefit works out.
     - of course, server failures are correlated for various reasons.

  Talk to people, read blogs, learn from case studies.

  Know your SLA. Sometimes it's okay for things to be broken for a little while (eg. batch processing).

* Know thy devs, you are their first POC
 - Know more than the devs
 - To unsure rather than wrong
 - Be wrong rather than nothing
 - Please learn a bunch so you can teach devs about critical stuff
   - Team specific maybe
   - know what application is good for what

* Know thy SOC, you are their first POC
  SOC's job is:
  - hardware upto OSish
    - eg. bonding
      - not to be confused with teaming
  - Windows servers (used primarily for MSSQL)

* App is not ours, technical aspects of the app are ours
 - keep team in the loop, esp
   - Team Lead
   - affected parties
 - reversible changes always except for unplanned
 - Know the business part of the app. know thy app.

* Look before you *** leap.
  Move slow, don't break things.
  Or move fast, fix things.

* You have two bosses
  - App Team Lead
  - DevOps Team Lead

    70/30 split of work

* We are on call on a rotating basis
  PagerDuty is your job for a given shift. You are the POC for that shift.

  Sometimes you get pinged in the middle of the night.
  Very occasionally things to go hell.

  Frequency: once in a month or so.


* Teams in Media.net:

** Content monetization
   System which brings ads from Yahoo and shows it to users
   they actually serve the content
   user facing
   designed to be fast
   cm has a spam filter (like a blocklist)

** Keyword black box (KBB)
   CM asks KBB what keywords to show stuff about
   KBB checks db for keywords, passes to CM

** Crawler
   KBB passes content to crawler, asks db what keywords does this content have to do with?
   
** Analyzer
   crawler passes analyzer content
   analyzer stores content in a db

** RTB (real time bidding)
   look at DFP, HeaderBidder
   replaced by Google's own platform, EBDI
   RTB does programmatic bidding

** Search engine monetization (SEM)
   Mkae legit sites, pocket publisher's cut.
   see blackfridayay.com

** Domain monetization
   typosquatting ads
   
** Mobile SDK
